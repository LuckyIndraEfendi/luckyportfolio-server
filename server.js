const express = require("express");
const nodemailer = require("nodemailer");
const cors = require("cors");
const dotenv = require("dotenv");
dotenv.config();
const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: process.env.MAIL_APPUSER ?? "",
    pass: process.env.MAIL_APPPASS ?? "",
  },
  secure: true,
  tls: { rejectUnauthorized: false },
});

const app = express();
app.use(
  cors({
    origin: process.env.PUB_FRONTEND ?? "",
    methods: ["POST"],
  })
);
app.use(express.json());
app.post("/api/email", (req, res) => {
  const mailData = {
    from: process.env.MAIL_APPUSER ?? "",
    to: "hayasaka592@gmail.com",
    subject: "New Message from Lucky Portfolio",
    html: `<!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>New Message from Lucky Portfolio</title>
      <style>
        body, p {
          margin: 0;
          padding: 0;
        }
    
        body {
          background-color: #f4f4f4;
          font-family: 'Arial', sans-serif;
        }
    
        /* Container for the email content */
        .email-container {
          max-width: 600px;
          margin: 20px auto;
          background-color: #ffffff;
          padding: 20px;
          border-radius: 8px;
          box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
    
        /* Style for the name */
        .name {
          font-size: 20px;
          font-weight: bold;
          color: #333333;
          margin-bottom: 10px;
        }
    
        /* Style for the message */
        .message {
          font-size: 16px;
          color: #555555;
          line-height: 1.5;
        }
      </style>
    </head>
    <body>
      <div class="email-container">
        <p class="name">Hai Nama saya ${req.body.firstName} ${req.body.lastName}</p>
        <p class="message">${req.body.message}</p>
        <p class="message">Contact me : ${req.body.email}</p>
      </div>
    </body>
    </html>`,
  };

  transporter.sendMail(mailData, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log({ message: "Mail send", message_id: info.messageId });
  });
});

app.listen(5000, () => {
  console.log(`Server listening on port 5000`);
});
